class Students {
    String name;
    private int age;
    private int flow;

    Students(String name, int age, int flow){
        this.name = name;
        this.age = age;
        this.flow = flow;
    }

    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Students that = (Students) o;

        if (flow != that.flow) return false;
        if (age != that.age) return false;
        if (name == null && that.name == null) {
            return true;
        } else if (name == null) {
            return false;
        } else {
            return name.equals(that.name);
        }
    }

    @Override
    public int hashCode(){
        int result = (name == null) ? 0 : name.hashCode();
        result = 31 * result + age;
        result = 31 * result + flow;
        return result;
    }
}
