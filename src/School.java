import java.util.HashMap;

public class School {

    private static HashMap<Students, Integer> mathematics = new HashMap<>();
    private static HashMap<Students, Integer> biologic = new HashMap<>();
    private static HashMap<Students, Integer> phisCulture = new HashMap<>();

    private void getGrades(Students student){
        if (mathematics.containsKey(student)) {
                int gradeM = mathematics.get(student);
                int gradeB = biologic.get(student);
                int gradeP = phisCulture.get(student);

                System.out.println("Оценки " + student.name + ":");
                System.out.println("По математике - " + gradeM);
                System.out.println("По биологии - " + gradeB);
                System.out.println("По физ-культуре - " + gradeP);
                System.out.println();
        } else {
            System.out.println("Такого ученика нет!");
        }
    }

    public static void main(String[] args) {

        School school = new School();

        Students max = new Students("Max", 24, 3);
        Students john = new Students("John", 20, 5);
        Students mike = new Students(null, 18, 1);

        mathematics.put(max, 3);
        mathematics.put(john, 5);
        mathematics.put(mike, 4);

        biologic.put(max, 5);
        biologic.put(john, 3);
        biologic.put(mike, 4);

        phisCulture.put(max, 4);
        phisCulture.put(john, 5);
        phisCulture.put(mike, 3);

        school.getGrades(new Students("null", 18, 1));
        school.getGrades(new Students("Max", 24, 3));
        school.getGrades(new Students("John", 20, 5));
    }
}
/*  Изначально, в первом варианте программы, при указании в методе getGrades существующего объекта класса(ключа),
   он просто выдавал нам оценку - значение мапы.
    После исправления, при указании в этом же методе новосозданного объекта, с такими же параметрами,
   происходит проверка ключа, hashcode'а и равенства объектов, далее выводиться на экран*/